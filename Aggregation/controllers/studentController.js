const colleges = require("../Models/collegesModel");
const student = require("../Models/studentsModel");
const marks = require("../Models/marksModel");
//-------------------------------------------------------
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;
const { response } = require("express");
//-------------------------------------------------------

//---------------PUT Colleges Data-------------------------
exports.postColleges = async (req, res) => {
  try {
    let result = await colleges.create(req.body);
    console.log(result);
    return res.send(result);
  } catch (error) {
    console.error("Error while fetching data:", error);
    res.status(500).send("Internal Server Error");
  }
};

//----------------PUT Student Details----------------------------
exports.inputStudent = async (req, res) => {
  try {
    const collegeId = req.params.collegeId;

    if (!mongoose.Types.ObjectId.isValid(collegeId)) {
      return res.status(400).send("Invalid collegeId");
    }
    const existingCollege = await colleges.findById(collegeId);
    if (!existingCollege) {
      return res.status(404).send("College not found");
    }
    const { firstName, lastName, email, dialCode, phone } = req.body;

    const newStudent = await student.create({
      collegeId: ObjectId(collegeId),
      firstName,
      lastName,
      email,
      dialCode,
      phone,
    });
    console.log("Student created:", newStudent);
    return res.status(201).send(newStudent);
  } catch (error) {
    console.error("Error while fetching data:", error);
    res.status(500).send("Internal Server Error");
  }
};

//----------------------PUT Marks-------------------------------------
exports.inputMarks = async (req, res) => {
  try {
    const studentId = req.params.studentId;

    if (!mongoose.Types.ObjectId.isValid(studentId)) {
      return res.status(400).send("Invalid studentId");
    }

    const existingStudent = await student.findById(studentId);
    if (!existingStudent) {
      return res.status(404).send("Student not found");
    }

    const { subject, marks: obtainedMarks } = req.body;

    const newMarks = await marks.create({
      studentId: ObjectId(studentId),
      subject,
      marks: obtainedMarks,
    });

    console.log("Marks Added:", newMarks);
    return res.status(201).send(newMarks);
  } catch (error) {
    console.error("Error while adding marks:", error);
    res.status(500).send("Internal Server Error");
  }
};

//-----------------------Get Details---------------------------------------------------------------
exports.getStudentDetails = async (req, res) => {
  try {
    const studentId = req.params.studentId;

    if (!mongoose.Types.ObjectId.isValid(studentId)) {
      return res.status(400).send("Invalid studentId");
    }

    const marksDetails = await marks
      .find({ studentId: studentId })
      .populate({
        path: "studentId",
        populate: { path: "collegeId" },
      })
      .select("subject marks");

    if (!marksDetails) {
      return res.status(404).send("Marks not found");
    }

    console.log("Marks Details:", marksDetails);
    return res.send(marksDetails);
  } catch (error) {
    console.error("Error while fetching marks details:", error);
    res.status(500).send("Internal Server Error");
  }
};

//-------------------------------------Aggregated Api--------------------------------------------
exports.getStudentDetailsWithAggregation = async (req, res) => {
  try {
    const studentId = req.params.studentId;

    if (!mongoose.Types.ObjectId.isValid(studentId)) {
      return res.status(400).send("Invalid studentId");
    }

    const aggregationResult = await marks.aggregate([
      {
        $match: {
          studentId: mongoose.Types.ObjectId(studentId),
        },
      },
      {
        $lookup: {
          from: "students",
          localField: "studentId",
          foreignField: "_id",
          as: "studentDetails",
        },
      },
      {
        $unwind: "$studentDetails",
      },
      {
        $lookup: {
          from: "colleges",
          localField: "studentDetails.collegeId",
          foreignField: "_id",
          as: "collegeDetails",
        },
      },
      {
        $unwind: "$collegeDetails",
      },
      {
        $project: {
          _id: "$studentDetails._id",
          firstName: "$studentDetails.firstName",
          lastName: "$studentDetails.lastName",
          email: "$studentDetails.email",
          dialCode: "$studentDetails.dialCode",
          phone: "$studentDetails.phone",
          collegeId: "$collegeDetails._id",
          collegeName: "$collegeDetails.collegeName",
          subject: 1,
          marks: 1,
        },
      },
    ]);

    if (!aggregationResult || aggregationResult.length === 0) {
      return res.status(404).send("Marks not found");
    }

    console.log("Student Details with Aggregation:", aggregationResult);
    return res.send(aggregationResult);
  } catch (error) {
    console.error(
      "Error while fetching student details with aggregation:",
      error
    );
    res.status(500).send("Internal Server Error");
  }
};
