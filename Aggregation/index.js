const express = require("express");
require("./conn");

const studentRouter = require("./Routes/studentRoutes");

const app = express();
app.use(express.json());

app.use("/api/v1/students", studentRouter);

app.listen(5000);
