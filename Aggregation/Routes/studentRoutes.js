const express = require("express");
const studentController = require('../controllers/studentController');
const router = express.Router();
const path = require("path");

//--------------Colleges Routes-------------------------------------------
router.route('/addColleges').post(studentController.postColleges);

//--------------Student Routes--------------------------------------------
router.route('/addStudents/:collegeId').post(studentController.inputStudent);

//--------------Marks Routes-----------------------------------------------
router.route('/addMarks/:studentId').post(studentController.inputMarks);


//------------Getting Student Details-------------------------------------------------------
router.route('/getStudentDetails/:studentId').get(studentController.getStudentDetails);

//-------------Aggregation Student Details-----------------------------------------------------------------
router.route('/aggregatedStudentDetails/:studentId').get(studentController.getStudentDetailsWithAggregation);



module.exports = router;
