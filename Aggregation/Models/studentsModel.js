const mongoose = require("mongoose");
const studentsSchema = new mongoose.Schema({
  collegeId: {
    type: mongoose.Types.ObjectId,
    ref: "colleges",
  },
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  dialCode: {
    type: String,
    required: true,
  },
  phone: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("students", studentsSchema);
