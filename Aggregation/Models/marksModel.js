const mongoose = require("mongoose");
const marksSchema = new mongoose.Schema({
  subject: {
    type: String,
    required: true,
  },
  marks: { type: Number, required: true },
  studentId: {
    type: mongoose.Types.ObjectId,
    ref: "students",
  },
});
module.exports = mongoose.model("marks", marksSchema);
