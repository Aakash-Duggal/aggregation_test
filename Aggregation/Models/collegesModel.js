const mongoose = require("mongoose");
const collegesSchema = new mongoose.Schema({
  collegeName: {
    type: String,
    required: true,
  },
});
module.exports = mongoose.model("colleges", collegesSchema);
